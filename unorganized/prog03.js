function Person() {
	/*
	this.age = age;
	this.name = name;
	*/
}

Person.prototype.constructor = function(args0, args1) {
	this.age = args0;
	this.name = args1;
};

var person1 = new Person();
var person2 = new Person(21);

console.log(person1.age);
console.log(person2.age);
