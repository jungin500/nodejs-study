var url = require('url');

var gurl = url.parse('https://www.google.co.kr/url?sa=t&rct=j&q=&esrc=s&source=web&cd=6&ved=0ahUKEwjp5tmQubvWAhUJKpQKHVjtC6cQFgg0MAU&url=https%3A%2F%2Fnamu.wiki%2Fw%2F%25EB%259D%25BC%25EC%259D%25B4%25EC%2596%25B8(%25EC%25B9%25B4%25EC%25B9%25B4%25EC%2598%25A4%25ED%2594%2584%25EB%25A0%258C%25EC%25A6%2588)&usg=AFQjCNGbZYyQyP9kzYW6PpMnry35Qy_yLg');

console.log('["url" 이용하여 파싱한 URL 객체]');
console.dir(gurl);

var querystring = require('querystring');
var qurl = querystring.parse(gurl.query);

console.log('\n["querystring" 이용하여 Query 파싱한 JSON 오브젝트]');
console.dir(qurl);