// EventEmitter

process.on('exit', function() {
	console.log("프로세스 종료 이벤트 트리거 됨");
});

console.log("2초후에 프로세스 종료를 시도합니다.");
setTimeout(function() {
	process.exit();
}, 2000);