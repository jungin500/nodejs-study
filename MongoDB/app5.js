var http = require('http'),
    express = require('express'),
    expressErrorHandler = require('express-error-handler'),
    session = require('express-session'),

    mongoose = require('mongoose'),
    crypto = require('crypto'),

    serveStatic = require('serve-static'),
    bodyParser = require('body-parser'),
    cookieParser = require('cookie-parser'),
    path = require('path');

/*
    Express 서버 구동
*/
var app = express();

app.set('port', process.env.PORT || 3000);
http.createServer(app).listen(app.get('port'), function () {
    console.log('서버 시작됨: %d', app.get('port'));

    connectDB();
});

var database, UserSchema, UserModel;

function connectDB() {
    var dbUrl = 'mongodb://localhost:27017/local';

    // mongoose로 연결
    console.log('DB 연결 시도...');

    mongoose.Promise = global.Promise;
    mongoose.connect(dbUrl, { useMongoClient: true });
    database = mongoose.connection;

    database.on('error', console.error.bind(console, 'mongoose 연결 오류'));
    database.on('open', function () {
        console.log('DB 연결 성공!');

        createUserSchema();

        database.on('disconnected', function () {
            console.log('DB 연결 끊어짐... 5초 뒤 재연결합니다.');
            setTimeout(connectDB, 5000);
        });
    });
}

var createUserSchema = function() {
    UserSchema = mongoose.Schema({
        id: { type: String, required: true, unique: true, 'default': ' ' },
        hashed_password: { type: String, required: true, unique: false, 'default': ' ' },
        salt: { type: String, required: true },
        name: { type: String, index: 'hashed', 'default': ' ' },
        age: { type: Number, 'default': -1 },
        created_at: { type: Date, index: { unique: false }, 'default': Date.now },
        updated_at: { type: Date, index: { unique: false }, 'default': Date.now },
    });

    UserSchema.virtual('password').set(function(password) {
        this._password = password;
        this.salt = this.makeSalt();
        this.hashed_password = this.encryptPassword(password);
        console.log('Virtual password 호출됨: ' + this.hashed_password);
    }).get(function() {
        return this._password;
    });

    UserSchema.method('encryptPassword', function(plainText, inSalt) {
        if(inSalt)
            return crypto.createHmac('sha1', inSalt).update(plainText).digest('hex');
        else
            return crypto.createHmac('sha1', this.salt).update(plainText).digest('hex');
    });

    UserSchema.method('makeSalt', function() {
        return Math.round((new Date().valueOf() * Math.random())) + '';
    });

    UserSchema.method('authenticate', function(plainText, inSalt, hashed_password) {
        if(inSalt) {
            console.log('authenticate() called -> %s : %s', this.encryptPassword(plainText, inSalt), hashed_password);
            return this.encryptPassword(plainText, inSalt) == hashed_password;
        } else {
            console.log('authenticate() called -> %s : %s', this.encryptPassword(plainText), hashed_password);
            return this.encryptPassword(plainText) == hashed_password;
        }
    });

    UserSchema.path('id').validate(function(id) {
        return id.length;
    }, 'id 칼럼의 값이 없습니다.');

    UserSchema.path('name').validate(function(name) {
        return name.length;
    }, 'name 칼럼의 값이 없습니다.');


    UserSchema.static('findById', function (id, callback) {
        return this.find({ id: id }, callback);
    });

    UserSchema.static('findAll', function (callback) {
        return this.find({}, callback);
    });

    console.log('스키마 정의 완료');

    UserModel = mongoose.model('users2', UserSchema);
    console.log('사용자 모델 정의 완료');
};

var addUser = function (database, data, callback) {
    console.log('addUser() 호출됨');

    var user = new UserModel(data);
    user.save(function (err) {
        if (err) {
            callback(err, null);
            return;
        }
        console.log('레코드 추가됨');
        callback(null, user);
    });
};

var authUser = function (database, id, pw, callback) {
    console.log('authUser() called');

    UserModel.findById(id, function (err, results) {
        if (err) {
            callback(err, null);
            return;
        }

        console.log('아이디 %s에 해당하는 User 검색 결과', id);
        // console.dir(results);

        if (results.length > 0) {
            console.log('아이디 %s에 해당하는 User 발견.', id);

            // 인증 과정에서는 사전에 만들어 둔 authenticate() 메소드 사용.
            var user = new UserModel({id: id});
            var authenticated = user.authenticate(pw, results[0]._doc.salt, results[0]._doc.hashed_password);

            if(authenticated) {
                console.log('비밀번호 일치함.');
                callback(null, results);
                return;
            } else {
                console.log('일치하지 않는 비밀번호');
                callback(null, null);
                return;
            }
        } else
            console.log('아이디 %s에 일치하는 사용자가 없음.', id);

        callback(null, null);
    });
};

/*
     라우터 설정
*/

var router = express.Router();

router.route('/').get(function (req, res) {
    console.log('[ROU#1] / 호출됨');

    res.redirect('/public/login.html');
});

router.route('/process/login').post(function (req, res) {
    console.log('[ROU#2] /process/login 호출됨');

    var paramId = req.query.id || req.body.id;
    var paramPw = req.query.pw || req.body.pw;

    if (database) {
        authUser(database, paramId, paramPw, function (err, docs) {
            if (err) throw err;

            if (docs) {
                // console.dir(docs[0]._doc);
                var username = docs[0].name;

                res.writeHead(200, { 'Content-Type': 'text/html; charset=utf8' });
                res.write('<meta charset="utf-8">');
                res.write('<h1>로그인 성공</h1><hr>');
                res.write('<h3>사용자 이름: ' + username + '</h3>');
                res.write('<a href="/public/login.html">로그인 페이지로 돌아가기</a>');
                res.end();
            } else {
                res.writeHead(200, { 'Content-Type': 'text/html; charset=utf8' });
                res.write('<meta charset="utf-8">');
                res.write('<h1>로그인 실패</h1><hr>');
                res.write('<a href="/public/login.html">로그인 페이지로 돌아가기</a>');
                res.end();
            }
        });
    }
});

router.route('/process/register').post(function (req, res) {
    console.log('[ROU#3] /process/register 호출됨');

    var paramName = req.query.name || req.body.name;
    var paramId = req.query.id || req.body.id;
    var paramPw = req.query.pw || req.body.pw;

    if (database) {
        addUser(database, { id: paramId, password: paramPw, name: paramName }, function (err, result) {
            if (err) throw err;

            if (result) {
                res.writeHead(200, { 'Content-Type': 'text/html; charset=utf8' });
                res.write('<h1>가입이 완료되었습니다.</h1>');
                res.write('<hr><a href="/public/index.html">홈으로 돌아가기</a>');
                res.end();
            } else {
                res.writeHead(200, { 'Content-Type': 'text/html; charset=utf8' });
                res.write('<h1>가입이 실패했습니다.</h1>');
                res.end();
            }
        });
    } else {
        res.writeHead(200, { 'Content-Type': 'text/html; charset=utf8' });
        res.write('<h1>DB 초기화 실패</h1>');
        res.end();
    }
});

router.route('/process/listuser').post(function (req, res) {
    console.log('[ROU#4] /process/listuser 호출됨');

    if (database) {
        UserModel.findAll(function (err, results) {
            if (err) {
                console.log('리스트 조회 중 오류 발생.');

                res.writeHead(200, { 'Content-Type': 'text/html; charset=utf8' });
                res.write('<h2>리스트 조회 중 오류 발생</h2>');
                res.end('<pre>' + err.stack + '</pre>');

                return;
            }

            if (results.length > 0) {
                // console.dir(results);

                res.writeHead(200, { 'Content-Type': 'text/html; charset=utf8' });
                res.write('<h2>사용자 리스트</h2>');

                res.write('<ol>');
                for (var i = 0; i < results.length; i++)
                    res.write('<li>' + i + '번째 사용자: ' + results[i]._doc.name + '</li>');
                res.end('</ol>');
            } else {
                res.writeHead(200, { 'Content-Type': 'text/html; charset=utf8' });
                res.write('<h2>리스트에 내용 없음</h2>');
                res.end();
            }
        });
    } else {
        res.writeHead(200, { 'Content-Type': 'text/html; charset=utf8' });
        res.end('<h2>DB 연결 안됨</h2>');
    }
});

/*
    미들웨어
*/

app.use(bodyParser.urlencoded({ extended: false }));
app.use(bodyParser.json());

app.use('/public', serveStatic(path.join(__dirname, 'public')));

app.use(router);

app.use(cookieParser());
app.use(session({
    secret: Date.now().toString(),
    resave: true,
    saveUninitialized: true
}));

// 항상 ExpressErrorHandler는 마지막에 호출되어야 한다!

var errorHandler = expressErrorHandler({
    static: {
        '404': './MongoDB/include/404.html'
    }
});

app.use(expressErrorHandler.httpError(404));
app.use(errorHandler);
