var http = require('http'),
    express = require('express'),
    expressErrorHandler = require('express-error-handler'),
    session = require('express-session'),

    MongoClient = require('mongodb').MongoClient,

    serveStatic = require('serve-static'),
    bodyParser = require('body-parser'),
    cookieParser = require('cookie-parser'),
    path = require('path');

var app = express();
var router = express.Router();
app.set('port', process.env.PORT || 3000);

var errorHandler = expressErrorHandler({
    static: {
        '404': './MongoDB/include/404.html'
    }
});

app.use(bodyParser.urlencoded({ extended: false }));
app.use(bodyParser.json());

app.use('/public', serveStatic(path.join(__dirname, 'public')));

app.use(router);

app.use(cookieParser());
app.use(session({
    secret: Date.now().toString(),
    resave: true,
    saveUninitialized: true
}));

router.route('/').get(function(req, res){ 
    console.log('[ROU#1] / 호출됨');

    res.redirect('/public/login.html');
});

router.route('/process/login').post(function(req, res) {
    console.log('[ROU#2] /process/login 호출됨');

    var paramId = req.query.id || req.body.id;
    var paramPw = req.query.pw || req.body.pw;

    if(database) {
        authUser(database, paramId, paramPw, function(err, docs) {
            if(err) throw err;

            if(docs) {
                console.dir(docs);
                var username = docs[0].name;

                res.writeHead(200, {'Content-Type': 'text/html; charset=utf8'});
                res.write('<h1>로그인 성공</h1><hr>');
                res.write('<h3>사용자 이름: ' + username + '</h3>');
                res.write('<a href="/public/login.html">로그인 페이지로 돌아가기</a>');
                res.end();
            } else {
                res.writeHead(200, {'Content-Type': 'text/html; charset=utf8'});
                res.write('<h1>로그인 실패</h1><hr>');
                res.write('<a href="/public/login.html">로그인 페이지로 돌아가기</a>');
                res.end();
            }
        });
    }

    // TODO 작성
});

var database;

function connectDB() {
    var dbUrl = 'mongodb://localhost:27017/local';
    MongoClient.connect(dbUrl, function(err, db) {
        if(err) throw err;

        console.log('DB에 연결되었습니다: ' + dbUrl);
        database = db;
    });
}

var authUser = function(database, id, pw, callback) {
    console.log('authUser() called');

    var users = database.collection('users');

    users.find({'id': id, 'password': pw}).toArray(function(err, docs) {
        if(err) {
            callback(err, null);
            return;
        }

        if(docs.length > 0) {
            console.log('아이디 %s, 비밀번호 %s에 해당하는 User 발견.', id, pw);
            callback(null, docs);
        } else {
            console.log('아이디 %s, 비밀번호 %s에 일치하는 사용자가 없음.', id, pw);
            callback(null, null);
        }
    });
};

// 항상 ExpressErrorHandler는 마지막에 호출되어야 한다!
app.use(expressErrorHandler.httpError(404));
app.use(errorHandler);

http.createServer(app).listen(app.get('port'), function() {
    console.log('서버 시작됨: %d', app.get('port'));

    connectDB();
});