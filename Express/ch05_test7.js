var fs = require('fs');
var http = require('http');

var server = http.createServer().listen(3000);

server.on('connection', function(socket){
    var addr = socket.address();
    console.log('[클라이언트 접속] %s:%d', addr.address, addr.port);
});

server.on('request', function(req, res) {
    console.log('[클라이언트 요청]')

    var filename = 'back.png';
    var infile = fs.createReadStream(filename, {flags: 'r'});
    var filelen = 0;
    var curlen = 0;

    fs.stat(filename, function(err, stats) {
        filelen = stats.size;
    });

    res.writeHead(200, {'Content-Type': 'image/png'});

    infile.on('readable', function() {
        var chunk;
        while(null != (chunk = infile.read())) {
            console.log('[파일 읽기] %dBytes Read', chunk.length);
            curlen += chunk.length;
            res.write(chunk, 'utf8', function(err) {
                if(err) throw err;
                console.log('[응답 부분 쓰기] %dBytes만큼 완료, 총 파일 크기 %dBytes (%d%%)', curlen, filelen, curlen / filelen * 100);
                if(curlen >= filelen)
                    res.end();  // 파일 크기보다 응답의 크기가 커지거나 같아지면 종료함.
            });
        }
    });
});