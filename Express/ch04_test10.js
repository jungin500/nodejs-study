var output = '안녕 1!';
var buffer1 = new Buffer(10);
var len = buffer1.write(output, 'utf8');
console.log('버퍼1 문자열: ' + buffer1.toString());

var buffer2 = new Buffer('안녕 2!', 'utf8');
console.log('버퍼2 문자열: ' + buffer2.toString());

console.log('버퍼1 객체 타입 == Buffer: ' + Buffer.isBuffer(buffer1));

var byteLen = Buffer.byteLength(output);
var str1 = buffer1.toString('utf8', 0, byteLen);
var str2 = buffer2.toString('utf8');

buffer1.copy(buffer2, 0, 0, len);
console.log('버퍼1 --> 버퍼2 복사후 문자열: ' + buffer2.toString('utf8'));

var buffer3 = Buffer.concat([buffer1, buffer2]);
console.log('버퍼1 | 버퍼2 붙인후 문자열: ' + buffer3.toString('utf8'));