var http = require('http');

var server = http.createServer();

var port = 3000;
server.listen(port, function() {
    console.log("Server started!");
});

server.on('connection', function(socket) {
    var addr = socket.address();
    console.log('[Client Connect] %s:%d', addr.address, addr.port);
});

server.on('request', function(req, res) {
    console.log('[Client Request]');
    console.dir(req);
    console.log('[Client Request End] %s', Date.now().toString());

    res.writeHead(200, {'Content-Type': 'text/html; charset=utf-8'});
    res.write('<title>응답 페이지</title>');
    res.write('<h1>Node.js로부터의 응답 페이지</h1>');
    res.end();
});

server.on('close', function() {
    console.log('[Server Closed]');
});