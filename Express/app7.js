var express = require('express'),
    path = require('path'),
    staticMid = require('serve-static'),
    bodyParser = require('body-parser'),
    http = require('http');

var app = express();
app.set('port', process.env.PORT || 3000);

http.createServer(app).listen(app.get('port'), function () {
    console.log('서버 시작됨: %d', app.get('port'));
});

app.use(bodyParser.urlencoded({ extended: false }));

app.use(bodyParser.json());

app.use('/public', staticMid(path.join(__dirname, 'public')));

app.use('/', function (req, res, next) {
    console.log('[MID#1] Handling event');

    var paramId = req.body.id || req.query.id;  // body는 POST, query는 GET
    var paramPw = req.body.pw || req.query.pw;

    res.writeHead(200, { 'Content-Type': 'text/html; charset=utf8' });
    res.write('<h1>Express 결과');
    res.write('<h4>paramId: ' + paramId + '</h4>');
    res.write('<h4>paramPw: ' + paramPw + '</h4>');
    res.end();

    console.log('[MID#1] Listing Body requests: ');
    console.dir(req.body);
    console.log('[MID#1] Listing Query requests: ');
    console.dir(req.query);
});