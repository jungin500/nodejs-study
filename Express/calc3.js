// Calculator Module

var util = require('util');
var EventEmitter = require('events').EventEmitter;

var Calc = function() {
	var self = this;
	
	self.on('stop', function() {
		console.log('Calc에 Stop 이벤트 전달됨.');	
	});
};

util.inherits(Calc, EventEmitter);

Calc.prototype.add = function(a, b) {
	return a + b;
};

Calc.prototype.subtract = function(a, b) {
	return a - b;
};

Calc.prototype.multiply = function(a, b) {
	return a * b;
};

module.exports = Calc;
module.exports.title = 'Calculator';