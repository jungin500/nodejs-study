var fs = require('fs');

var infile = fs.createReadStream('./output.txt', {flags: 'r'});
var outfile = fs.createWriteStream('./output2.txt', {flags: 'w'});

infile.on('data', function(data) {
	console.log("output.txt -> output2.txt 데이터 받음: %s", data);
	
	outfile.write(data);
});

infile.on('end', function() {
	outfile.end(function() {
		console.log("데이터 쓰기 끝. 무결성 검증 중!");
	});
	
	var verify_in = fs.createReadStream('./output.txt', {flags: 'r'});
	var verify_out = fs.createReadStream('./output2.txt', {flags: 'r'});
	
	var verify1, verify2;
	
	verify_in.on('data', function(data) {
		verify1 = data;
		
		verify_out.on('data', function(data) {
			verify2 = data;
			
			verify_out.on('end', function() {
				if(verify1.equals(verify2))
					console.log("무결성 검증 성공!");
				else {
					console.log("무결성 검증 실패. 파일 비교중");
					console.log("output.txt: %s", verify1);
					console.log("output2.txt: %s", verify2);
				}
			});
		});
	});
});