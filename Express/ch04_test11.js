var fs = require('fs');

var infile = fs.createReadStream('./output.txt', {flags: 'r'});
var outfile = fs.createWriteStream('./output2.txt', {flags: 'w'});

infile.on('data', function(data) {
	console.log("output.txt에서 읽어들인 데이터:  %s\n\t--> output2.txt", data);
	outfile.write(data);
});

infile.on('end', function(data) {
	console.log('output.txt: 파일 읽기 종료');
	
	outfile.end(function() {
		console.log('output2.txt: 파일 쓰기 종료');
	});
});