var net = require('net');

var server = net.createServer(function(socket) {
    socket.write('Echo Server! Write some lines...\r\n');
    socket.pipe(socket);
    
    socket.on('data', function(data) {
       console.log("[Data Received] %s", data); 
    });
})

server.listen(8081, '0.0.0.0');