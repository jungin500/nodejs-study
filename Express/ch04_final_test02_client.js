var net = require('net');

var client = new net.Socket();
client.connect(8081, '127.0.0.1', function() {
    console.log("Connected!");
})

client.on('data', function(data) {
    console.log("[Data Received] %s", data);
})

client.on('close', function() {
    console.log('Connection Closed!');
})

setTimeout(function() {
    client.write("Hello! this is Client. Great.");
}, 1000);