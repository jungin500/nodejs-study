var express = require('express'),
    http = require('http');

var app = express();
app.set('port', process.env.PORT || 3000);

http.createServer(app).listen(app.get('port'), function () {
    console.log('서버 시작됨: %d', app.get('port'));
});

app.use(function (req, res, next) {
    console.log('[MID#1] Handling event');

    // TODO: 내용 입력
});