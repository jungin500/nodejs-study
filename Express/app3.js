var express = require('express'),
    http = require('http');

var app = express();

app.set('port', process.env.PORT || 3000);

http.createServer(app).listen(app.get('port'), function() {
    console.log("HTTP 서버가 시작되었습니다. 포트: %d", app.get('port'));
});

app.use(function(req, res, next) {
    console.log('[MID] 1번째 호출');
    req.user = 'jungin500';
    next();
});

app.use('/', function(req, res, next) {
    console.log('[MID] 2번째 호출');
    res.writeHead(200, {'Content-Type': 'text/html; charset=utf8'});
    res.end('<h1>사용자 이름은 ' + req.user + '입니다.</h1>');
});

app.use('/request', function(req, res, next) {
    console.log('[MID] 3번째 호출');
    res.writeHead(200, {'Content-Type': 'text/html; charset=utf8'});
    res.end('<h1>요청한 사용자 이름은 ' + req.user + '입니다.</h1>');
});