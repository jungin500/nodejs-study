var http = require('http');

var server = http.createServer();

var port = 3000;
server.listen(port, function() {
    console.log("Server started!");
});

server.on('connection', function(socket) {
    var addr = socket.address();
    console.log('[Client Connect] %s:%d', addr.address, addr.port);
});

server.on('request', function(req, res) {
    console.log('[Client Request]');
    console.dir(req);
    console.log('[Client Request End] %s', Date.now());
});

server.on('close', function() {
    console.log('[Server Closed]');
});