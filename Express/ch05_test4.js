var http = require('http');

http.createServer(function(req, res) {
    console.log('[Client Request]');
    console.dir(req);
    console.log('[Client Request End] %s', Date.now().toString());

    res.writeHead(200, {'Content-Type': 'text/html; charset=utf-8'});
    res.write('<title>응답 페이지</title>');
    res.write('<h1>단순 표현</h1>');
    res.end();
}).listen(3000, function() {
    console.log('웹서버 실행 중');
});