var http = require('http');
var fs = require('fs');

var server = http.createServer().listen(3000);

server.on('connection', function(socket) {
    var addr = socket.address();
    console.log('[클라이언트 접속] %s:%d', addr.address, addr.port);
});

server.on('request', function(req, res) {
    console.log('[클라이언트 요청]');

    var infile = 'back.png';
    var rs = fs.createReadStream(infile, {flags: 'r'});

    rs.pipe(res);
});