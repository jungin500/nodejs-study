var fs = require('fs');

var infile = './output.txt';
var outfile = './output2.txt';

fs.exists(outfile, function(exists) {
	if(exists) {
		fs.unlink(outfile, function(err) {
			if(err) throw err;
			console.log("%s를 쓰기 전에 삭제했습니다.", outfile);
		});
		
		var inStream = fs.createReadStream(infile, {flags: 'r'});
		var outStream = fs.createWriteStream(outfile, {flags: 'w'});
		inStream.pipe(outStream);
		console.log('파일 복사 중: %s --> %s', infile, outfile);
	}
});