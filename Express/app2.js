var express = require('express'),
    http = require('http');

var app = express();

app.use(function(req, res, next) {
    console.log('MiddleWare #1 Request');

    res.writeHead(200, {'Content-Type': 'text/html; charset=utf-8'});
    res.end('Hello Express!');
});

http.createServer(app).listen(3000, function() {
    console.log('Express Server started');
})