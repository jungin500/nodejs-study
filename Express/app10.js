var http = require('http'),
    express = require('express'),
    expressErrorHandler = require('express-error-handler'),
    fs = require('fs'),
    multer = require('multer'),
    cors = require('cors'),
    path = require('path'),
    staticPath = require('serve-static'),
    bodyParser = require('body-parser'),
    cookieParser = require('cookie-parser'),
    expressSession = require('express-session');

var app = express();
app.set('port', process.env.PORT || 3000);

var storage = multer.diskStorage({
    destination: function(req, file, callback) {
        callback(null, 'uploads');
    },
    filename: function(req, file, callback) {
        callback(null, file.originalname + Date.now());
    }
});

var upload = multer({
    storage: storage,
    limits: {
        files: 10,
        fileSize: 1024 * 1024 * 1024
    }
});

app.use(bodyParser.urlencoded({ extended: false }));
app.use(bodyParser.json());

app.use('/', staticPath(path.join(__dirname, 'public')));
app.use('/uploads', staticPath(path.join(__dirname, 'uploads')));

app.use(cookieParser());
app.use(expressSession({
    secret: 'esession123kdf' + Date.now(),
    resave: true,
    saveUninitialized: true
}));

app.use(cors());

var router = express.Router();

router.route('/process/memo').post(upload.array('photo', 1), function(req, res) {
    console.log('[MID#1] Called');

    var paramAuthor = req.query.author || req.body.author;
    var paramTimestamp = req.query.timestamp || req.body.timestamp;
    var paramDescription = req.query.description || req.body.description;

    try {
        var files = req.files;

        res.writeHead(200, {'Content-Type': 'text/html; charset=utf8'});
        for(var i = 0; i < files.length; i++) {
            res.write('<img src="/uploads/' + files[i].filename + '"></img>');
            res.write('<p>' + '/uploads/' + files[i].filename + '</p>');
        }

        res.write('<p>입력된 값</p>');
        res.write('<p>작성자: ' + paramAuthor + '</p>');
        res.write('<p>작성시간: ' + paramTimestamp + '</p>');
        res.write('<p>내용: ' + paramDescription + '</p>');
        res.end('<button onclick="javascript:history.back()">다시 작성</button>');
    } catch (err) {
        console.dir(err.stack);
    }
});

app.use('/', router);

var errorHandler = expressErrorHandler({
    staticPath: {
        '404': './public/404.html'
    }
});

app.use(expressErrorHandler.httpError(404));
app.use(errorHandler);

http.createServer(app).listen(app.get('port'), function() {
    console.log('서버 시작됨: %d', app.get('port'));
});
