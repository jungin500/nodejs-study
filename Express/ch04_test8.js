var fs = require('fs');

fs.open('./output.txt', 'w', function(err, fd) {
	if(err) throw err;
	
	var buf = new Buffer("1234567890a");
	fs.write(fd, buf, 0, buf.length, null, function(err, written, buffer) {
		if(err) throw err;
		
		console.log("파일 쓰기 완료: ", err, written, buffer);
		
		
		fs.close(fd, function() {
			console.log("파일 닫기 완료.");
		});
		
		console.log("파일 닫기 요청 완료");
	});
	console.log("파일 쓰기 요청 완료.");
});
console.log("파일 열기 요청 완료");