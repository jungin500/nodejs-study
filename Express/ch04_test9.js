var fs = require('fs');

fs.open('./output.txt', 'r', function(err, fd) {
	if(err) throw err;
	
	var buf = new Buffer(10);
	console.log("버퍼 타입 isBuffer=%s", Buffer.isBuffer(buf));
	
	fs.read(fd, buf, 0, buf.length, null, function(err, bytesRead, buffer) {
		if(err) throw err;
		
		var inStr = buffer.toString('utf8', 0, bytesRead);
		console.log('읽은 데이터: %s', inStr);
		
		console.log(err, bytesRead, buffer);
		
		fs.close(fd, function() {
			console.log("파일을 닫았습니다.");
		});
	});
});