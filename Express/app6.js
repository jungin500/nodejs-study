var express = require('express'),
    http = require('http');

var app = express();
app.set('port', process.env.PORT || 3000);

http.createServer(app).listen(app.get('port'), function () {
    console.log('서버 시작됨: %d', app.get('port'));
});

app.use(function (req, res, next) {
    console.log('[MID#1] Handling event');

    var userAgent = req.header('User-Agent');
    var paramName = req.query.name;

    res.writeHead('200', {'Content-Type': 'text/html; utf8'});
    res.write('<h1>Express 결과');
    res.write('<h4>userAgent: ' + userAgent + '</h4>');
    res.write('<h4>paramName: ' + paramName + '</h4>');
    res.end();

    console.log('[MID#1] Listing Query: ');
    console.dir(req.query);
});