var http = require('http');

var server = http.createServer().listen(3000);

var options = {
    host: 'www.google.co.kr',
    port: 80,
    path: '/'
};

var req = http.get(options, function(res) {
    var resData = '';
    res.on('data', function(chunk) {
        resData += chunk;
    });

    res.on('end', function() {
        server.on('request', function(req, res) {
            res.writeHead(200, {'Content-Type': 'text/html; charset=utf-8'});
            res.write(resData);
            res.end();
        });
    });
});

req.on('error', function(err) {
    console.log('호스트 %s 읽는 중 오류 발생: %s', options.host, err.message);
});