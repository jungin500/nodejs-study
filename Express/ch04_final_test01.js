var fs = require('fs');

fs.readFile('./ch04_final_test01_input.txt', 'utf8', function(err, data) {
    if(err) throw err;
    
    var dataset = data.split('\n');
    for(var i = 0; i < dataset.length; i++) {
        var col = dataset[i].split(' ');
        console.log("Username: %s", col[0]);
    }
});