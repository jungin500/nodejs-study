// EventEmitter

process.on('tick', function(count) {
	console.log('tick 이벤트 발생. 데이터: ' + count);
});

setTimeout(function() {
	process.emit('tick', 1020);
}, 2000);