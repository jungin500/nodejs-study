var http = require('http');
var fs = require('fs');

var server = http.createServer().listen(3000);

server.on('request', function(req, res) {
    console.log('[알림] 요청 받음. %s', new Date().toString());

    var filename = 'back.png';
    fs.readFile(filename, function(err, data) {
        res.writeHead(200, {'Content-Type': 'image/png'});
        res.write(data);
        res.end();
    });
});
